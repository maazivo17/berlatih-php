<?php
function tukar_besar_kecil($string)
{
    //kode di sini
    $result = "";
    $arr = str_split($string);
    for ($i = 0; $i < count($arr); $i++) {
        if (ord($arr[$i]) >= 65 && ord($arr[$i]) <= 90) {
            $result .= strtolower($arr[$i]);
        } else {
            $result .= strtoupper($arr[$i]);
        }
    }
    echo "<br>";
    return $result;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
