<?php
function ubah_huruf($string)
{
    //kode di sini
    $word = "";
    $arr1 = str_split($string);
    for ($a = 0; $a < count($arr1); $a++) {
        $batch = ord($arr1[$a]) + 1;
        $word .= chr($batch);
    }
    echo "<br>";
    return $word;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
